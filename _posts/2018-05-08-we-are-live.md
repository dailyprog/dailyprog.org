---
title: "We're live!"
layout: default
author: satou
date: 2018-05-08
---

Since today, we're live here. We're currently working on our website.


<img src="/img/working.jpg" width="128" />

{% highlight c %}
#include<stdio.h>

int main()
{
  if (fork())
    printf("I execute!\n");
  else
    printf("So do I!\n");
  
  return 0;
}
{% endhighlight %}

