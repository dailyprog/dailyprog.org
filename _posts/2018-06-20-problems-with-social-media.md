---
title: "Problems With Social Media"
layout: default
author: mabynogy
date: 2018-06-20
---

There is very little social about social media. The major sites should just be branded as media sites. A social site would be one where you go to interact with people you know. I think the disconnect comes from the options for interaction that we are presented with. Almost all the sites offer a cop out 'like' interaction. This is not a social interaction, yet, these social media sites revolve around it, they base their algorithms on it.

Facebook has strongly diverged from what people believe it to be. Most people still think of it as the place where you put personal pictures for friends and family. However, it is actually the media site that a foreign power used to manipulate the 2016 US election. If it was just a place to share personal stuff with personal people this would not be possible. It is possible because it is simply a media portal with built in social proof. The heart of the content is not the original uploads from your friends, but their likes, shares, and interactions with corporate media. This leads to an endless stream of news, memes, and viral videos, and to me, it feels like shit to browse.

Twitter is another resource of consumption, but in a different way. I am not sure Twitter is supposed to be, but it largely feels like a broadcast platform with very little interaction. Twitter's display algorithm strongly favors celebrity accounts, that is, accounts with many followers that receive a lot of interaction. Even when you do not follow large accounts, it will still display them via the shit-your-friend-liked or person-your-friend-follows algorithm. Due to the volume of interaction these popular accounts receive, they can not reciprocate. This creates one-sided interactions which, to me, also feel like shit to participate in.

IRC is the tool that I've found that breaks these mechanisms. The communities are generally small. You get to actually interact with people. You get to know them through these interactions. This creates a meaningful connections. The icing on the cake is that people are not selling you things, you're not being tracked (at least for advertising purposes), and no one is worried about how engaged you are with the platform.

IRC does not give me a mechanism to talk to friends and family though, as they are not on IRC. I would love to have a site where I could post real time updates about my life so people could meet up with me. I want a place to share major changes and accomplishments in my life, post photos of my girlfriend and dogs. There is no solution to this that doesn't involve user surveilance and data brokerage. I would happily pay 20 dollars a year for a social media site that does not track or advertise if anyone is up for building it.


HN comments [news.ycombinator.com/item?id=17145530](https://news.ycombinator.com/item?id=17145530)  
Reproduced from [dailyprog.org/~tacixat/Problems With Social Media.html](http://dailyprog.org/~tacixat/blog/posts/Problems%20With%20Social%20Media.html)  
Courtesy [tacixat](/~tacixat/)  
