---
title: "Mediacenter: a live coded distributed content network"
layout: default
author: mabynogy
date: 2018-06-21
---

Kenster works hard on improving his Mediacenter project. 
Mediacenter is a distributed content network with group collaboration in mind.

You can follow Kenster's progess through his live coding sessions:
 - [on YT](https://www.youtube.com/user/KingHerring)
 - [on Twitch](https://www.twitch.tv/kingherring)
 - [homepage](/~kenster/)


## Group list

<img src="/img/mediacenter-screenshot1.png" />


## Group page

<img src="/img/mediacenter-screenshot2.png" />


## User profile

<img src="/img/mediacenter-screenshot3.png" />
