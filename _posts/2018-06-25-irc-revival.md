---
title: "IRC revival"
layout: default
author: mabynogy
date: 2018-06-25
---

## irc.com

Andrew Lee who is the owner of a well-known VPN provider recently bought 
irc.com. In a post, he is calling for an IRC revival with a serie of 
proposals (including funding).

- the post: [irc.com/](http://irc.com/)
- HN comments: [news.ycombinator.com/item?id=17375831](https://news.ycombinator.com/item?id=17375831)
- 4chan /g thread: [boards.4chan.org/g/thread/66464646](http://boards.4chan.org/g/thread/66464646)
- reddit comments: [reddit.com/r/opensource](https://www.reddit.com/r/opensource/comments/8t8j9c/irccom_acquired_by_london_trust_media_plans_for_a/e15ne5a/)


## IRC clients

buovjaga showed us a preview of the next release of kiwiirc, a new and
interesting web-client made in Go (Dispatch) and the new Konversation
in preparation.

- kiwiirc preview: [kiwiirc.com/nextclient](https://kiwiirc.com/nextclient/)
- Dispatch demo: [dispatch.khlieng.com/connect](https://dispatch.khlieng.com/connect)
- HN comments about Dispatch: [news.ycombinator.com/item?id=17333014](https://news.ycombinator.com/item?id=17333014)
- Konversation new features: [blogs.kde.org/konversation-2x-2018-new-user-interface-matrix-support-mobile-version](https://blogs.kde.org/2017/09/05/konversation-2x-2018-new-user-interface-matrix-support-mobile-version)
