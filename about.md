---
title: About
layout: default
menu: main
permalink: /about/
---

We are a programming community based on 4chan's `/g/dpt` thread.
We enjoy sharing ideas and talking about everything programming and technology related.

To stay in touch with us:
 - [gitlab](https://gitlab.com/dailyprog)
 - [irc](/chat/)

