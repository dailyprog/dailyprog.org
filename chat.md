---
title: Chat
layout: default
menu: main
permalink: /chat/
---

Join us on `#/g/dpt` on rizon.


<table>
 <tr>
  <td>
   <h3>irc client</h3>
   <div><code>/server irc.rizon.net</code></div>
   <div><code>/join #/g/dpt</code></div>
  </td>
  <td>
   <h3>webchat</h3>
   <div>Full page link: <a href="https://kiwiirc.com/client/irc.rizon.net?channels=#/g/dpt" target="_blank">kiwiirc.com/irc.rizon.net?channels=#/g/dpt</a></div>
   <div>Another webchat: <a href="https://qchat.rizon.net/?channels=#/g/dpt">rizon.net/channels=#/g/dpt</a></div>
  </td>
 </tr>
</table>

<div class="spacer"></div>

<iframe src="https://kiwiirc.com/client/irc.rizon.net/?nick=prog|?#/g/dpt" width="100%" class="border" style="height:120vh"></iframe>
