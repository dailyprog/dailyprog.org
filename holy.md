---
title: Holy programmers
layout: default
permalink: /holy/
---

## Richard Stallman

<a name="rms"></a>
<img src="/img/holy-rms.png" />

- [wikipedia.org/Richard_Stallman](https://en.wikipedia.org/wiki/Richard_Stallman)
- 4chan /g emblem: [4chan.org/g](http://boards.4chan.org/g/)


## Terry A. Davis

<a name="terry"></a>
<img src="/img/holy-terry.png" />

- [wikipedia.org/TempleOS](https://en.wikipedia.org/wiki/TempleOS)  
- image created by [hfc](http://dailyprog.org/~hfc/)


## Paul Graham

<a name="pg"></a>
<img src="/img/pg-halo.png" />

- [wikipedia.org/Paul_Graham](https://en.wikipedia.org/wiki/Paul_Graham_(programmer))
- Beating the Averages: [paulgraham.com/avg.html](http://www.paulgraham.com/avg.html)
- image created by [hfc](http://dailyprog.org/~hfc/)


## Elon Musk

<a name="musk"></a>
<img src="/img/musk.png" />

- [wikipedia.org/Elon_Musk](https://en.wikipedia.org/wiki/Elon_Musk)  
- image created by [hfc](http://dailyprog.org/~hfc/)

