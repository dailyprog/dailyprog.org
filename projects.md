---
title: Projects
layout: default
menu: main
permalink: /projects/
---

Keep in mind some of those projects are works in progress.


## [kenster](http://dailyprog.org/~kenster/)

- [TinyCDN](https://github.com/kennymalac/tinyCDN): a small and fast CDN "middleware"
- [mediacenter](https://github.com/kennymalac/mediacenter): a web application that hosts a registration server for viewing and uploading user content


## [mabynogy](http://dailyprog.org/~mabynogy/)

- [honor](http://dailyprog.org/~mabynogy/honor/): hacking stuff around tacixat's idea
- baka: coffeescript-like programming language


## [Satou](http://dailyprog.org/~satou/)

- [itsumi](https://gitlab.com/alexm98/itsumi/): an IRC bot written in PHP
- [ipfs-mediashare](https://gitlab.com/alexm98/ipfs-mediashare): mediasharing system over IPFS
- [cl-ipfs](https://gitlab.com/alexm98/cl-ipfs): Common Lisp implementation of an IPFS API client


## [tacixat](http://dailyprog.org/~tacixat/)

- [blog](http://dailyprog.org/~tacixat/blog/)
- [honor demo](http://dailyprog.org:8888/money): a payment platform using qr codes
