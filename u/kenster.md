---
title: "kenster"
layout: default
permalink: /u/kenster/
---
- name "kenster"
- directory "/home/kenster"
- url "/~kenster"
- usage "6.8M"
- cloc
  - lang "JavaScript"
  - extension "es6"
  - count "2 068"
  - data
    - HTML
      - nFiles "204"
      - blank "349"
      - comment "1 150"
      - code "12 868"
    - JavaScript
      - nFiles "63"
      - blank "82"
      - comment "112"
      - code "2 068"
    - CSS
      - nFiles "3"
      - blank "307"
      - comment "80"
      - code "1 481"