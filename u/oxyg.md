---
title: "oxyg"
layout: default
permalink: /u/oxyg/
---
- name "oxyg"
- directory "/home/oxyg"
- url "/~oxyg"
- usage "116K"
- cloc
  - lang "Python"
  - extension "py"
  - count "192"
  - data
    - HTML
      - nFiles "7"
      - blank "65"
      - comment "6"
      - code "257"
    - Python
      - nFiles "7"
      - blank "72"
      - comment "55"
      - code "192"