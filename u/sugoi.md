---
title: "sugoi"
layout: default
permalink: /u/sugoi/
---
- name "sugoi"
- directory "/home/sugoi"
- url "/~sugoi"
- usage "68K"
- cloc
  - lang "C"
  - extension "c"
  - count "10"
  - data
    - HTML
      - nFiles "2"
      - blank "5"
      - comment "37"
      - code "44"
    - CSS
      - nFiles "1"
      - blank "2"
      - comment "0"
      - code "12"
    - C
      - nFiles "1"
      - blank "2"
      - comment "0"
      - code "10"